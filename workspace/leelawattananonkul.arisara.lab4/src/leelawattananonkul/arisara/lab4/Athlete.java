package leelawattananonkul.arisara.lab4;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Athlete {

	private String name;
	private LocalDate birthdate;
	Gender gender;	
	private double weight;
	private double height;
	private String nationality;
	
	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
	LocalDate halloween =LocalDate.parse("31-10-2017", formatter);

	public Athlete(String name,double weight, double height, Gender gender, String nationality ,String birthdate) {
		super();
		this.name = name;
		this.weight = weight;
		this.height = height;
		this.gender = gender;
		this.nationality = nationality;
		this.birthdate = LocalDate.parse(birthdate, formatter);
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDate getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(LocalDate birthdate) {
		this.birthdate = birthdate;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	@Override
	public String toString() {
		return "Athlete [" + name +  weight + "kg, " + height +"m, " + gender+ "," + nationality + ", " + birthdate  
				+ "]";
	}
}