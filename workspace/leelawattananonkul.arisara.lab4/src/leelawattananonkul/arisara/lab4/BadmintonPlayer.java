package leelawattananonkul.arisara.lab4;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class BadmintonPlayer extends Athlete {
	static String sport = " Badminton" ;
	private double racketLength;
	private int worldRanking;
	
	public BadmintonPlayer(String name, double weight, double height, Gender gender, String nationality,
			String birthdate, double racketLength, int worldRanking) {
		super(name, weight, height, gender, nationality, birthdate);
		this.racketLength = racketLength;
		this.worldRanking = worldRanking;
	}
	
	public static String getSport() {
		return sport;
	}

	public static void setSport(String sport) {
		BadmintonPlayer.sport = sport;
	}

	public double getRacketLength() {
		return racketLength;
	}

	public void setRacketLength(double racketLength) {
		this.racketLength = racketLength;
	}

	public int getWorldRanking() {
		return worldRanking;
	}

	public void setWorldRanking(int worldRanking) {
		this.worldRanking = worldRanking;
	}

	@Override
	public String toString() {
		return "BadmintonPlayer [racketLength=" + racketLength + ", worldRanking=" + worldRanking + "]";
	}
	public int compareAge() {
		LocalDate dateBefore;
		LocalDate dateAfter;
		int year = (int)ChronoUnit.YEARS.between(dateBefore, dateAfter);
		return year;
	}
}
