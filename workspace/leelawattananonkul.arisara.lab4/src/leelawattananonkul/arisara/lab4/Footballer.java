package leelawattananonkul.arisara.lab4;

public class Footballer extends Athlete{
	
	static String sport = " Footballer ";
	private String position;
	private String team;
	
	public Footballer(String name, double weight, double height, Gender gender, String nationality, String birthdate,
			String position, String team) {
		super(name, weight, height, gender, nationality, birthdate);
		this.position = position;
		this.team = team;
	}
	
	public static String getSport() {
		return sport;
	}

	public static void setSport(String sport) {
		Footballer.sport = sport;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getTeam() {
		return team;
	}

	public void setTeam(String team) {
		this.team = team;
	}

	@Override
	public String toString() {
		return "Footballer [position=" + position + ", team=" + team + "]";
	}
}
